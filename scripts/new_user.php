#!/usr/bin/env php
<?php

define('SPOONS', 7657176);
define('FORKS', 7661092);
define('LEVEL_MAINTAINER', 40);
define('LEVEL_DEVELOPER', 30);
define('LEVEL_REPORTER', 20);

use GuzzleHttp\Client;

require 'vendor/autoload.php';

if (!$project_id = $argv[1]) {
  throw new Exception('Missing Project ID.');
}
if (!$issue_iid = $argv[2]) {
  throw new Exception('Missing issue ID.');
}
if (!$token = getenv('GITLAB_TOKEN')) {
  throw new Exception('Missing Gitlab token.');
}

$config = [
  'headers' => ['Authorization' => 'Bearer '. $token],
  'http_errors' => true,
//  'debug' => true,
  'base_uri' => 'https://gitlab.com/api/v4/',
];
$client = new Client($config);

// Get issue data.
$response = $client->get("projects/$project_id/issues/$issue_iid", []);
$body = $response->getBody();
$issue = json_decode($body);
$author_id = $issue->author->id;
$author_name = $issue->author->username;

logg("Creating memberships for $author_name");
#https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
$params = [
  'user_id' => $author_id,
  'access_level' => LEVEL_REPORTER,
];
$response = $client->post('groups/' . SPOONS . '/members', ['form_params' => $params]);
$params['access_level'] = LEVEL_DEVELOPER;
$response = $client->post('groups/' . FORKS . '/members', ['form_params' => $params]);

$pipeline_url = getenv('CI_PIPELINE_URL');
// https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
logg("Post the closing comment to project $project_id, issue $issue_iid");
$body = <<<EOT
Welcome to DrupalSpoons. You may now add/edit issues, or commit to any project in the [DrupalForks](https://gitlab.com/drupalforks) group. A few tips:

   - Clone projects from their _DrupalForks_ repo. For example, see the blue _Clone_ button in [Devel](https://gitlab.com/drupalforks/devel), for example.
   - Merge requests are used to promote code from DrupalForks to its upstream in DrupalSpoons.
   - Lots more info is in the [README](https://gitlab.com/drupalspoons/webmasters/-/blob/master/README.md).
   - If you want to preserve your git checkouts for projects that have moved to DrupalSpoons, run `git remote set-url origin https://gitlab.com/drupalforks/[SLUG].git`. Replace [_SLUG_] with the project machine name. You will now push and pull from DrupalForks instead of git.drupalcode.org.
   - [Migrate a contrib project to DrupalSpoons](https://gitlab.com/drupalspoons/webmasters/-/blob/master/docs/onboarding_project.md)

$pipeline_url
EOT;
$response = $client->post("projects/$project_id/issues/$issue_iid/notes", ['form_params' => ['body' => $body]]);

logg("Edit and close project $project_id, issue $issue_iid.");
// https://docs.gitlab.com/ee/api/issues.html#edit-issue
$params = [
  'state_event' => 'close',
  'assignee_id' => $author_id,
  'add_labels' => 'migration',
];
$response = $client->put("projects/$project_id/issues/$issue_iid", ['form_params' => $params]);
logg('The END.');

function logg($message) {
  fwrite(STDERR, "$message\n");
}




