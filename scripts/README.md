#### New User
new_user.php
 - Used by new_user job in gitlab-ci.yml (which gets triggerred by a webhook).
 - Requires PROJECT_ID variable.
 - Requires ISSUE_ID variable.

#### New Project
new_project.php
 - Requires USER_NAME variable.
 - Requires SLUG variable.
