Any project may request that their drupal.org code and issues be copied to Gitlab.

#### Drupal.org Project Maintainer
1. Prerequisite: The d.o. project maintainer (must have *all* maintainer permissions) must [be a member of DrupalSpoons](onboarding_user.md).
1. The d.o. maintainer [creates an issue in this 'Webmasters' project](https://gitlab.com/drupalspoons/webmasters/-/issues/new?issue[title]=Migrate%20PROJECT_MACHINE_NAME%20to%20DrupalSpoons). Issue title should be _Migrate [PROJECT MACHINE NAME] to DrupalSpoons_ (replace with your project's machine name, the last part of the URL on drupal.org) and it should have the label ~migration. No issue body is needed.
1. While a DrupalSpoons admin follows the steps below, please ask your co-maintainers to become Gitlab.com members (if needed), and send you their usernames. Later, you will use this information to give them project maintainer permissions.

#### DrupalSpoons Admin
1. A [DrupalSpoons](https://gitlab.com/drupalspoons) group maintainer assigns the issue to self.
1. Add a _thumbs up_ to the issue. 
1. Verify that the initiator has full Maintainer rights to the project on drupal.org
1. Browse to [Run Pipeline](https://gitlab.com/drupalspoons/webmasters/pipelines/new?var[USER_NAME]&var[SLUG]):
    - `Run for`: master

    | Key | Value | Notes |
    | ------ | ------ | ----- |
    | USER_NAME | [Gitlab username] | Omit the `@` at start of the username. |
    | SLUG | [drupal.org project machine name] | e.g. ctools, pathauto |
1. Verify that the Pipeline succeeds, including [issue migration](https://gitlab.com/drupalspoons/drupal-issue-migration).
1. Assign issue to the initiator. This grants them an issue credit per #2. 
1. **Close** the Webmasters issue with the following comment, replacing [SLUG]:
```

    Your project has been migrated to DrupalSpoons. Browse to [:spoon:](https://gitlab.com/drupalspoons/[SLUG]) and [:fork_and_knife:](https://gitlab.com/drupalforks/[SLUG]). Next steps:

   - Invite each of your active co-maintainers via https://gitlab.com/groups/drupaladmins/[SLUG]/-/group_members. You will need their Gitlab username or their email address. It's OK to postpone this step for any that you don't know. They should get the `Owner` or `Developer` role. `Owner` has the added permission to add/remove project maintainers. Both roles get the same permissions in the DrupalSpoons project.
   - Optional. Mark recent d.o. issues as _Closed (Duplicate)_ and post a link to their DrupalSpoons equivalent.
   - While editing your drupal.org project, uncheck `Enable issue tracker`. Its in the Issues section of the vertical tabs element (scroll down).
   - Grant `Write to VCS` permission to the [drupalspoons](https://www.drupal.org/u/drupalspoons) user account (example screenshot in !83). DrupalSpoons will then instantly mirror code changes to git.drupal.org.
   - Remove `Write to VCS` permission for everyone except [drupalspoons](https://www.drupal.org/u/drupalspoons). This prevents accidental pushes to git.drupal.org.
   - Optional. Configure [DrupalSpoons CI](https://gitlab.com/drupalspoons/webmasters/-/blob/master/docs/ci.md).
   - Remove daily or weekly automated testing in the drupal.org. This saves the DA money.
   - Edit your drupal.org project description to read:

    <h3>Merge Requests, Issues, and CI for this project may be found at https://gitlab.com/drupalspoons/[SLUG].</h3>
    <ul>
        <li>Checkout a new clone: <code>git clone https://gitlab.com/drupalforks/[SLUG].git</code></li>
        <li>*Or* update an existing clone if you have local branches you want to preserve: <code>git remote set-url origin https://gitlab.com/drupalforks/[SLUG].git</code></li>
    </ul>

```
