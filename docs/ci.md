#### Getting Started

1. Add a `.gitlab-ci.yml` in the project root dir. [Copy from Devel](https://gitlab.com/drupalspoons/devel/-/blob/4.x/.gitlab-ci.yml). Commit and push the file.
2. Pipelines should start running after each push. Click the __CI/CD__ link to see them.

#### Customizing
1. You may add services to your .gitlab-ci.yml file if you want to test with memcache, elasticsearch, redis, etc.
1. You may list patches as per usual in composer.json if you need changes in other packages (for example).
1. Optional. Go to CI/CD => Schedules and add any important build permutations to a weekly schedule. Example permutations would be a non-default Drupal core version, DB backend, etc. See _Build Matrix_ below.

#### Build Matrix
Use the _Run Pipeline_ button on your Pipelines listing to test any branch with alternate versions of Drupal core, PHP version, or
DB driver. The recognized variables and default values are:

| Name                   | Default |
|------------------------|---------|
| DRUPAL_CORE_CONSTRAINT | ^8      |
| PHP_TAG                | 7.3     |
| DB_DRIVER              | mysql   |
| MARIADB_TAG            | 10.3    |
| POSTGRES_TAG           | 10.5    |

- DB_DRIVER recognizes `mysql`, `sqlite`, or `pgsql`.

#### Run tests locally
- Run all tests - `vendor/bin/spoon unit`. See the `<scripts>` section of composer.spoons.json to learn what that does.
- You may append arguments and options to Composer scripts: `vendor/bin/spoon unit -- --filter testDevelGenerateUsers`
- Run a suite: `vendor/bin/spoon unit -- --testsuite functional`
- Skip slow tests: `vendor/bin/spoon unit -- --exclude-group slow`
- Use a different URL: `SIMPLETEST_BASE_URL=http://example.com vendor/bin/spoon unit`
