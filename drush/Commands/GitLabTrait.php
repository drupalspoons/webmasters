<?php
namespace Drush\Commands;

use GuzzleHttp\Client;

trait GitLabTrait {

  public function getClient() {
    if (!$token = getenv('GITLAB_TOKEN')) {
      throw new \Exception('Missing Gitlab token.');
    }
    $config = [
      'headers' => ['Authorization' => 'Bearer '. $token],
      'http_errors' => true,
      //  'debug' => true,
      'base_uri' => 'https://gitlab.com/api/v4/',
    ];
    return new Client($config);
  }
}
