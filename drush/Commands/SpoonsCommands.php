<?php
namespace Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;

class SpoonsCommands extends DrushCommands {

  use GitLabTrait;

  public const DRUPALADMINS = '7835720';

  /**
   * Get all project slugs.
   *
   * @command mentions
   * @field-labels
   *   id: Id
   *   name: Name
   * @usage drush mentions --field=name | pbcopy
   *   Get all group names onto the clipboard.
   *
   * @param $options
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function mentions($options = ['format' => 'table']) {
    $client = $this->getClient();
    $params = [
      'all_available' => true,
      'order_by' => 'path',
      // @todo paging.
      'per_page' => 99,
    ];
    // https://docs.gitlab.com/ee/api/groups.html#list-a-groups-subgroups
    $response = $client->get('groups/' . self::DRUPALADMINS . '/subgroups', ['query' => $params]);
    $groups = json_decode($response->getBody());
    foreach ($groups as $group) {
      $rows[] = ['id' => $group->id, 'name' => "@drupaladmins/$group->path"];
    }
    return new RowsOfFields($rows);
  }

}
