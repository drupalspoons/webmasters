<?php

namespace Drupal\Tests\webmasters\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the JavaScript functionality of the Webmasters module.
 *
 * @group webmasters
 */
class WebmastersTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['webmasters'];

  protected $defaultTheme = 'stark';

  /**
   * Test callback.
   */
  public function testSomething() {
    // Let's test password strength widget.
    $this->config('user.settings')
      ->set('verify_mail', FALSE)
      ->save();

    $this->drupalGet('user/register');

    $page = $this->getSession()->getPage();
  }  

}
