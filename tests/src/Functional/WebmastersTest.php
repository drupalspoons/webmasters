<?php

namespace Drupal\Tests\webmasters\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group webmasters
 */
class WebmastersTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['webmasters'];

  protected $defaultTheme = 'stark';

  /**
   * Test callback.
   */
  public function testSomething() {
    $admin_user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Administration"]');
  }

}
