<?php
/**
 * GitLab Web Hook
 * See https://gitlab.com/kpobococ/gitlab-webhook/-/blob/master/gitlab-webhook-push.php
 *
 * This script should be placed within the web root of your desired deploy
 * location. The GitLab repository should then be configured to call it for the
 * "Push events" trigger via the Web Hooks settings page.
 *
 */

// CONFIGURATION
// =============

/* Hook script location. The hook script is a simple shell script that executes
 * the actual git push. Make sure the script is either outside the web root or
 * inaccessible from the web
 *
 * This setting is REQUIRED
 */
$hookfile = 'scripts/mirror.sh';

/* Log file location. Log file has both this script's and shell script's output.
 * Make sure PHP can write to the location of the log file, otherwise no log
 * will be created!
 *
 * This setting is REQUIRED
 */
$logfile = 'php://stderr';

/* Hook password. If set, this password should be passed as a GET parameter to
 * this script on every call, otherwise the hook won't be executed.
 *
 * This setting is RECOMMENDED
 */
$password = getenv('WEBHOOK_PWD_MIRROR');

$http_password = getenv('HTTP_X_GITLAB_TOKEN');

/* Ref name. This limits the hook to only execute the shell script if a push
 * event was generated for a certain ref (most commonly - a master branch).
 *
 * Can also be an array of refs:
 *
 *     $ref = array('refs/heads/master', 'refs/heads/develop');
 *
 * This setting does not support the actual refspec, so the refs should match
 * exactly.
 *
 * See http://git-scm.com/book/en/Git-Internals-The-Refspec for more info on
 * the subject of Refspec
 *
 * This setting is OPTIONAL
 */
#$ref = 'refs/heads/master';

// THE ACTUAL SCRIPT
// -----------------
// You shouldn't edit beyond this point,
// unless you know what you're doing
// =====================================
function log_append($message, $time = null)
{
  global $logfile, $gitlab_token;

  // MW: Mask secrets.
  $message = str_replace($gitlab_token, '*****', $message);

  $time = $time === null ? time() : $time;
  $date = date('Y-m-d H:i:s');
  $pre  = $date . ' (' . $_SERVER['REMOTE_ADDR'] . '): ';

  file_put_contents($logfile, $pre . $message . "\n", FILE_APPEND);
}

function exec_command($command)
{
  $output = array();

  exec($command, $output);

  foreach ($output as $line) {
    log_append('SHELL: ' . $line);
  }
}

if (isset($password))
{
  if (empty($http_password)) {
    log_append('Missing hook password.');
    die();
  }

  if ($http_password !== $password) {
    log_append('Invalid hook password..');
    die();
  }
}

// GitLab sends the json as raw post data
$input = file_get_contents("php://input");
$json  = json_decode($input);

if (!is_object($json) || empty($json->ref)) {
  log_append('Invalid push event data');
  die();
}

if (isset($ref))
{
  $_refs = (array) $ref;

  if ($ref !== '*' && !in_array($json->ref, $_refs)) {
    log_append('Ignoring ref ' . $json->ref);
    die();
  }
}
$slug = basename($json->project->path_with_namespace);

log_append("Mirror to  drupalforks/$slug");
$gitlab_token = getenv('GITLAB_TOKEN');
$push_creds = 'oauth2:' . $gitlab_token . '@';
$commandline = "$hookfile https://gitlab.com/drupalspoons/$slug.git https://${push_creds}gitlab.com/drupalforks/$slug.git";
$output = exec_command($commandline);
log_append($output);

log_append("Mirror to git.drupal.org/project/$slug");
exec_command('scripts/write_key.sh');
$commandline = "$hookfile https://gitlab.com/drupalspoons/$slug.git git@git.drupal.org:project/$slug.git";
$output = exec_command($commandline);
log_append($output);
